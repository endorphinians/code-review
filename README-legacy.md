# Use API4AI Code Review as a CI/CD template (legacy)

⚠️**IMPORTANT:** This section describes how to set up code review in a "legacy" way. If you are looking to set up code review as a [GitLab CI/CD component](https://docs.gitlab.com/ee/ci/components/index.html) (generally available in GitLab 17.0), please refer to the ["Use as a CI/CD component"](README-component.md) document.


## 🚀 Quick start

1. Define the following CI/CD variables in your project (or group or GitLab instance) [settings](https://docs.gitlab.com/ee/ci/variables/#define-a-cicd-variable-in-the-ui):
  * `A4A_API_KEY` with an API key provided to you by the API4AI team.
  * `A4A_CODE_REVIEW_GITLAB_TOKEN` with your GitLab access token ([personal](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html), [project](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html), or [group](https://docs.gitlab.com/ee/user/group/settings/group_access_tokens.html)). The token must have at least developer-level permissions with the "api" scope.  
    💡 **Tip:** We recommend giving it a nice name, such as `API4AI Code Reviewer`, because code review comments will be authored by GitLab Bot under this name.
2. In your [project's settings](https://docs.gitlab.com/ee/user/project/settings/), make sure that CI/CD is enabled and that at least one GitLab runner is available.
3. Create (or modify if it exists) a `.gitlab-ci.yml` file in the root of your repository by adding a review stage to the stages definition and including the `a4a-code-review` template using the `include` keyword.
  ```yaml
  stages:
    - review

  include:
    - remote: 'https://gitlab.com/api4ai/code-review/-/raw/1.0.1/templates/a4a-code-review/template.yml'
  ```


## 📄 Conventions

1. The job runs only for merge requests.
2. Review results are added as comments to the files.
3. If the commit message contains any of message includes one of:  
  `[no_review]`, `[skip_review]`, `[no review]`, `[skip review]`, `[no-review]`, `[skip-review]`, `[mute]`,  
  `:no_review:`, `:skip_review:`, `:no review:`, `:skip review:`, `:no-review:`, `:skip-review:`, `:mute:`,  
  the review job is not triggered.


## 🛠 Configuration

The code review job can be configured via environment variables in two alternative ways:

* [Set CI/CD Variables in the GitLab UI settings](https://docs.gitlab.com/ee/ci/variables/#define-a-cicd-variable-in-the-ui) for a project, group, or even a GitLab instance (if you use a self-hosted solution).
* [Extend the included job definition](https://docs.gitlab.com/ee/ci/yaml/includes.html) by deep merging. This means that the configuration of an included job can be performed by declaring a local job with the same name and redefining options.

💡 **Tip:** We recommend setting variables that contain secret information (API keys, tokens, etc.) via the GitLab UI settings and enabling "mask mode" for them to prevent leakage.

See the following lists for the settings that influence jobs defined in this template.

### Credentials

* `A4A_API_KEY` defines an API4AI key for accessing the API.
* `A4A_CODE_REVIEW_GITLAB_TOKEN` is a GitLab [personal](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html), [project](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html), or [group](https://docs.gitlab.com/ee/user/group/settings/group_access_tokens.html) access token used to interact with GitLab via API. Developer-level permission with "api" scope is required.

### Other options

* `A4A_CODE_REVIEW_TARGET` defines a regex pattern for matching files.  
By default, the target regex is `\.(py|c|h|cpp|hpp|cs|sh|(j|t)sx?)$` which enables the review for files with the following extensions:
  - Bash/Shell: `.sh`
  - C#: `.cs`
  - C/C++: `.h`, `.hpp`, `.c`, `.cpp`
  - JavaScript: `.js`, `.jsx`, `.mjs`, `.mjsx`, `.ts`, `.tsx`
  - Python: `.py`
* `A4A_CODE_REVIEW_TARGET_EXTRA` defines an additional regex pattern for matching files. Useful if you don't want to redefine `A4A_CODE_REVIEW_TARGET`.  
By default, it is empty.
* `A4A_CODE_REVIEW_TARGET_EXCLUDE` defines a regex pattern to exclude files from review (even if they are matched by `A4A_CODE_REVIEW_TARGET` or `A4A_CODE_REVIEW_TARGET_EXTRA`). Useful if you don't want to redefine `A4A_CODE_REVIEW_TARGET`.  
By default, it is empty.
* `A4A_CODE_REVIEW_LANGUAGE` defines the review language.  
By default, review comments will be produced in `English`.

💡 **Tip:** When you define CI/CD variables via the UI, pay attention to the "Expand variable reference" option (which is enabled by default). It may cause undesired behavior when the `$` character is used in the value (which is common, for example, if your value is a RegEx pattern).

### Example

Example of configuration by deep merging:

```yaml
stages:
  - review

include:
  - remote: 'https://gitlab.com/api4ai/code-review/-/raw/1.0.1/templates/a4a-code-review/template.yml'

a4a-code-review:
  variables:
    A4A_CODE_REVIEW_TARGET_EXTRA: '\.json$'
```


## ▶️ Manual execution

For some reasons, you may wish to control the code review job execution manually. For those purposes, simply include the `template-manual.yml` file instead of `template.yml`:

```yaml
stages:
  - review

include:
  - remote: 'https://gitlab.com/api4ai/code-review/-/raw/1.0.1/templates/a4a-code-review/template-manual.yml'
```


## 📌 Pin version

All the examples above refer to a specific CI/CD job version. If you wish to refer to the latest version, simply use `main` in the template URL instead of the specified release name (e.g., `1.0.1`). Example:

```yaml
stages:
  - review

include:
  - remote: 'https://gitlab.com/api4ai/code-review/-/raw/main/templates/a4a-code-review/template.yml'
```

# API4AI Code Review

This project implements GitLab CI/CD templates to perform code reviews for merge requests in your projects using API from API4AI.

<div align="center">
<a target="_blank" href="https://api4.ai?utm_source=ocr_example_repo&utm_medium=readme&utm_campaign=examples"><img src="https://storage.googleapis.com/api4ai-static/logo/a4a-logo-horizontal-gradient-rectangular-bg-round-glow-small-550.png"/></a>
<br>
<a target="_blank" href="https://www.instagram.com/api4ai"><img src="https://img.shields.io/badge/instagram--blue?style=social&logo=instagram"/></a>
<a target="_blank" href="https://www.facebook.com/api4ai.solutions/"><img src="https://img.shields.io/badge/facebook--blue?style=social&logo=facebook"/></a>
<a target="_blank" href="https://twitter.com/Api4Ai"><img src="https://img.shields.io/badge/twitter--blue?style=social&logo=twitter"/></a>
<a target="_blank" href="https://www.linkedin.com/company/api4ai"><img src="https://img.shields.io/badge/linkedin--blue?style=social&logo=linkedin"/></a>
</div>


## 📙 Documentation

⚠️**IMPORTANT:** The templates in this project can be used either as a [GitLab CI/CD component](https://docs.gitlab.com/ee/ci/components/index.html) or by using the "legacy" `include:remote` syntax. Starting from version 17.0, GitLab has made the use of CI/CD components generally available. Thus if your project, where you want to set up a code review, is hosted on `gitlab.com`, then we highly recommend using it in the modern way as a "CI/CD component." If your project is hosted on a self-hosted GitLab, consider the "legacy" method.

👉 [Use as a CI/CD component](README-component.md)  
👉 [Use as a CI/CD template (legacy)](README-legacy.md)


## 📩 Contacts

Feel free to contact API4AI team if have any questions.

* Email: hello@api4.ai
* Telegram: https://t.me/a4a_support_bot

# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).



## Unreleased
Add unreleased changes below. Do not remove this line.



## [1.0.1] - 2024-06-10

### Added

Limit the concurrent run of review jobs by utilizing GitLab resource groups.

[1.0.1]: https://gitlab.com/api4ai/code-review/-/compare/1.0.0...1.0.1



## 1.0.0 - 2024-05-15

### Added

The first release of CI/CD components/templates.

The `a4a-code-review` CI/CD job is implemented – a job to review code using API4AI. It automatically performs code reviews for merge requests and leaves comments.
